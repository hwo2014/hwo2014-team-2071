package noobbot;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.lang.reflect.Type;
import java.net.Socket;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

import noobbot.GameInit.Race.Car.ID;
import noobbot.GameInit.Race.Track.Piece;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import com.google.gson.annotations.SerializedName;
import com.google.gson.reflect.TypeToken;

public class Main {
    public static void main(String... args) throws IOException {
        String host = args[0];
        int port = Integer.parseInt(args[1]);
        String botName = args[2];
        String botKey = args[3];

        System.out.println("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

        final Socket socket = new Socket(host, port);
        final PrintWriter writer = new PrintWriter(new OutputStreamWriter(socket.getOutputStream(), "utf-8"));

        final BufferedReader reader = new BufferedReader(new InputStreamReader(socket.getInputStream(), "utf-8"));

        new Main(reader, writer, new Join(botName, botKey));
    }

    final Gson gson;
    private PrintWriter writer;
	private YourCar myCar;
	private GameInit gameInfo;
	private Map<Integer,String> optimalChanges;
	private PiecePosition previousPosition;
	private double deceleration = 0.30;
	private double friction = 0.46;
	private TurboInfo turboInfo;
	private int lastStraightStart;
	private Set<Integer> turboPieces;
	private int lastTurboUsageTicket;

    public Main(final BufferedReader reader, final PrintWriter writer, final Join join) throws IOException {
        this.writer = writer;
        String line = null;
        
        GsonBuilder builder = new GsonBuilder();
        builder.registerTypeAdapter(MsgWrapper.class, new MsgWrapperDeserializer());
        gson = builder.create();
        optimalChanges = new HashMap<>();
        turboPieces = new HashSet<>();

//        BotID botID = new BotID();
//        botID.name = "Eetu";
//        botID.key = "pQ7tKp/IljG3Mg";
//        
//        NewRace newRace = new NewRace();
//        newRace.botId = botID;
//        newRace.carCount = 1;
//        newRace.trackName = "germany";
//        newRace.password = "s3cret";
        send(join);
//        send(new JoinRace(newRace));

        while((line = reader.readLine()) != null) {
            final MsgWrapper msgFromServer = gson.fromJson(line, MsgWrapper.class);
            int gameTick = gson.fromJson(line, GameTick.class).gameTick;
            if (msgFromServer.msgType.equals("carPositions")) {
            	List<CarPosition> positions = (List<CarPosition>)msgFromServer.data;
            	CarPosition ownCar = getOwnCarPosition(positions);
            	String change = checkAndSwitchLane(positions);
            	if(change != null) {
            		send(new SwitchLane(change));
            	}
            	else if(this.turboInfo != null && this.lastTurboUsageTicket + this.turboInfo.turboDurationTicks < gameTick && isTurboStraight(positions)) {
            		this.lastTurboUsageTicket = gameTick;
            		send(new Turbo("Pedal to the medal! Turbo on!"));
            	} else {
            		send(new Throttle(getThrottle(positions)));
            	}
            	
            } else if (msgFromServer.msgType.equals("join")) {
                System.out.println("Joined");
            } else if (msgFromServer.msgType.equals("gameInit")) {
                this.gameInfo = ((GameInit)msgFromServer.data);
                setupGame();
            	System.out.println("Race init");
            } else if (msgFromServer.msgType.equals("gameEnd")) {
                System.out.println("Race end");
            } else if (msgFromServer.msgType.equals("crash")) {
                System.out.println(((Car)msgFromServer.data).color + " has crashed");
            } else if (msgFromServer.msgType.equals("dnf")) {
                System.out.println(((DNF)msgFromServer.data).car.color + " has been disqualified for: " + ((DNF)msgFromServer.data).reason);
            } else if (msgFromServer.msgType.equals("spawn")) {
                System.out.println(((Car)msgFromServer.data).color + " has spawned");
            } else if (msgFromServer.msgType.equals("lapFinished")) {
                if(((LapFinished)msgFromServer.data).car.color.equals(this.myCar.color))
                	setOptimalChanges();
            } else if (msgFromServer.msgType.equals("turboAvailable")) {
            	this.turboInfo = (TurboInfo)msgFromServer.data;
                System.out.println("Turbo available");
            } else if (msgFromServer.msgType.equals("finish")) {
                System.out.println(((Car)msgFromServer.data).color + ", " +
                		((Car)msgFromServer.data).name + " finished");
            } else if (msgFromServer.msgType.equals("gameStart")) {
                System.out.println("Race start");
            } else if (msgFromServer.msgType.equals("yourCar")) {
            	// Get color of our car
            	this.myCar = ((YourCar)msgFromServer.data);
            } else {
                send(new Ping());
            }
        }
    }

    private void setOptimalChanges() {
    	this.optimalChanges.put(2, "Right");
    	this.optimalChanges.put(7, "Left");
    	this.optimalChanges.put(15, "Right");
	}

	private boolean isLastLap(CarPosition ownCar) {
		if(ownCar.piecePosition.lap + 1 == this.gameInfo.race.raceSession.laps)
			return true;
		
		return false;
	}

	private boolean isTurboStraight(List<CarPosition> positions) {
    	for(CarPosition pos : positions) {
			if(pos.id.color.equals(this.myCar.color)) {
				if(this.turboPieces.contains(pos.piecePosition.pieceIndex))
					return true;
					
			}
    	}
    	
    	return false;
	}

	private void setupGame() {
    	// Set up optimal lanes
//    	int pieceIndex = 0;
//    	List<Integer> previousStraight = new ArrayList<>();
//		for(Piece piece : this.gameInfo.race.track.pieces) {
//			if(piece.angle > 22.5) {
//				for(Integer straight : previousStraight)
//					this.optimalChanges.put(straight, "Left");
//				previousStraight.clear();
//			} else if(piece.angle < 22.5) {
//				for(Integer straight : previousStraight)
//					this.optimalChanges.put(straight, "Right");
//				previousStraight.clear();
//			} else {
//				previousStraight.add(pieceIndex);
//			}
//			pieceIndex++;
//		}
    	
    	setOptimalChanges();
    	
    	int index = 0;
    	int lastStraightStart = 0;
    	boolean reset = true;
    	for(Piece piece : this.gameInfo.race.track.pieces) {
    		if(piece.angle == 0.0 && reset) {
    			lastStraightStart = index;
    			reset = false;
    		} else if(piece.angle != 0.0)
    			reset = true;
    			
    		index++;
    	}
    	
    	//this.turboPieces.add(34);
    	this.turboPieces.add(35);
    	this.turboPieces.add(36);
    	this.turboPieces.add(37);
    	this.turboPieces.add(38);
    	//this.turboPieces.add(39);
    	
//    	this.turboPieces.add(54);
//    	this.turboPieces.add(55);
//    	this.turboPieces.add(56);
    	
    	this.lastStraightStart = lastStraightStart;
    	
    	
		
		this.previousPosition = new PiecePosition();
	}
	
	private CarPosition getOwnCarPosition(List<CarPosition> positions) {
		for(CarPosition pos : positions)
			if(pos.id.color.equals(this.myCar.color))
				return pos;
		
		return null;
	}

	private String checkAndSwitchLane(List<CarPosition> positions) {
		String change = null;
		for(CarPosition pos : positions) {
			if(pos.id.color.equals(this.myCar.color)) {
				if(pos.piecePosition.lane.startLaneIndex == pos.piecePosition.lane.endLaneIndex) {
					change = this.optimalChanges.remove(pos.piecePosition.pieceIndex);
					break;
				}
			}
		}
		return change;
	}

	private double getThrottle(List<CarPosition> data) {
    	
    	// Start with a default throttle
    	double throttle = 1.0;
    	double angle = 0.0;
    	int piece = 0;
    	double pieceDis = 0.0;
    	double speed = 0.0;
    	
		for(CarPosition pos : data) {
			if(pos.id.color.equals(this.myCar.color)) {
				
				speed = getCurrentSpeed(pos.piecePosition);
				
//				Piece current = getPiece(pos.piecePosition.pieceIndex);
//				Piece next = getPiece(pos.piecePosition.pieceIndex + 1);
//				Piece nextNext = getPiece(pos.piecePosition.pieceIndex + 2);
				
//				CurveDegree currentDegree = null;
//				CurveDegree nextDegree = null;
//				CurveDegree nextNextDegree = null;
//				
//				if(current.angle != 0.0 && current.radius != 0.0)
//					currentDegree = getCurveDegree(current.angle,current.radius,getLaneOffset(pos.piecePosition.lane.startLaneIndex));
//				
//				if(next.angle != 0.0 && next.radius != 0.0)
//					nextDegree = getCurveDegree(next.angle,next.radius,getLaneOffset(pos.piecePosition.lane.endLaneIndex));
//				
//				if(nextNext.angle != 0.0 && nextNext.radius != 0.0)
//					nextNextDegree = getCurveDegree(nextNext.angle,nextNext.radius,getLaneOffset(pos.piecePosition.lane.endLaneIndex));
				
				angle = pos.angle;
				piece = pos.piecePosition.pieceIndex;
				pieceDis = pos.piecePosition.inPieceDistance;
				
				for(int i = 0; i < 6; i++) {
					if(needsBreaking(pos.piecePosition,i,speed)) {
						throttle = 0.0;
						break;
					}
				}
				
				//Override in qualification last round for top speed
				if(isLastStraight(pos.piecePosition)) {
					throttle = 1.0;
					
				}
					
				
				
				
//				if(currentDegree != null) {
//					switch (currentDegree) {
//					case ultra:
//						if(speed > 6.2)
//							throttle = 0.0;
//						else
//							throttle = 0.65;
//						break;
//					case hard:
//						if(speed > 7.5)
//							throttle = 0.0;
//						else
//							throttle = 0.7;
//						break;
//					case medium:
//						throttle = 1.0;
//						break;
//					case easy:
//						throttle = 1.0;
//						break;
//					}
//				if(speed > 8  && nextNext.radius >= 100) {
//					System.out.println("pls stop right now!!!");
//					throttle = 0.1;
//				} else if(speed > getMaxSpeed(next.radius)  && next.radius >= 100) {
//					System.out.println("pls stop!!!");
//					throttle = 0.1;
//				} else if(pos.piecePosition.inPieceDistance > (0.3 * current.length) && nextDegree == CurveDegree.hard) {
//					System.out.println("Hard curve ahead! No throttle");
//					throttle = 0.2;
//				} else if(pos.piecePosition.inPieceDistance > (0.3 * current.length) && nextDegree == CurveDegree.medium) {
//					throttle = 0.5;
//				} else if(pos.piecePosition.inPieceDistance > (0.3 * current.length) && nextDegree == CurveDegree.easy) {
//					throttle = 0.8;
//				} else {
//					System.out.println("No curves ahead. Full throttle.");
//					throttle = 1.0;
//				}
				
				// Save the position
				this.previousPosition = pos.piecePosition;
			}
		}
		
		System.out.println("throttle: " + throttle + " angle: " + angle  + " piece: " + piece  + " pieceDis: " + pieceDis   + " speed: " + speed);
			
		return throttle;
	}
    
    private boolean isLastStraight(PiecePosition piecePosition) {
    	if(piecePosition.lap == this.gameInfo.race.raceSession.laps)
    		if(piecePosition.pieceIndex + 1 == this.lastStraightStart)
    			return true;
    			
		return false;
	}

	private boolean needsBreaking(PiecePosition current, int offset, double speed) {
    	double laneOffset = getLaneOffset(current.lane.endLaneIndex);
    	Piece targetPiece = getPiece(current.pieceIndex + offset);
    	double effectiveRadius;
    	
    	if(targetPiece.angle > 0.0)
    		effectiveRadius = targetPiece.radius - laneOffset;
    	else if(targetPiece.angle < 0.0)
    		effectiveRadius = targetPiece.radius + laneOffset;
    	else return false;
    	
    	return getMaxSpeed(effectiveRadius) < 
    	(speed - (this.deceleration * (getDistance(current,offset) / speed)));
	}

	private double getDistance(PiecePosition piecePosition, int offset) {
		if(offset == 0)
			return 0.0;
		else if(offset == 1)
			return getRealLength(getPiece(piecePosition.pieceIndex),piecePosition.lane.endLaneIndex) - piecePosition.inPieceDistance;
		else
			return getRealLength(getPiece(piecePosition.pieceIndex + offset - 1),piecePosition.lane.endLaneIndex) + getDistance(piecePosition, offset - 1);
	}

	private double getRealLength(Piece piece, int endLaneIndex) {
		if(piece.length != 0.0)
			return piece.length;
		else {
			double effectiveRadius;
			
			if(piece.angle > 0.0)
				effectiveRadius = piece.radius - getLaneOffset(endLaneIndex);
			else
				effectiveRadius = piece.radius + getLaneOffset(endLaneIndex);
			
			return (Math.abs(piece.angle) / 360) * 2 * Math.PI * effectiveRadius;
		}
	}

	private double getMaxSpeed(double radius) {
		return Math.sqrt(radius * this.friction);
	}

	private double getCurrentSpeed(PiecePosition piecePosition) {
		if(this.previousPosition.pieceIndex == piecePosition.pieceIndex)
			return piecePosition.inPieceDistance - this.previousPosition.inPieceDistance;
		else {
			double previousGap = getDistance(this.previousPosition, 1);
			return piecePosition.inPieceDistance + previousGap;
		}
	}

	private double getLaneOffset(int laneIndex) {
    	double offset = 0.0;
    	
		for(noobbot.GameInit.Race.Lane lane : this.gameInfo.race.track.lanes)
			if(laneIndex == lane.index) {
				offset = lane.distanceFromCenter;
				break;
			}
		
		return offset;
				
	}

	enum CurveDegree {
    	easy,medium,hard,ultra
    }

	private CurveDegree getCurveDegree(double angle, double radius, double laneOffset) {
		
		double realRadius;
		// Corver to the right
		if(angle > 0)
			realRadius = radius - laneOffset;
		else
			realRadius = radius + laneOffset;
		
		double degree = Math.abs(angle) / realRadius;
		
		if(degree >= 0.45)
			return CurveDegree.ultra;
		else if(degree >= 0.35)
			return CurveDegree.hard;
		else if(degree >= 0.15)
			return CurveDegree.medium;
		else
			return CurveDegree.easy;
	}

	/**
	 * Gets the piece for a given index.
	 * 
	 * @param pieceIndex
	 * @return
	 */
	private Piece getPiece(int pieceIndex) {
		int realIndex = pieceIndex % this.gameInfo.race.track.pieces.size();
		return this.gameInfo.race.track.pieces.get(realIndex);
	}

	private void send(final SendMsg msg) {
        writer.println(msg.toJson());
        writer.flush();
    }
}

class GameTick {
	public int gameTick;
}

abstract class SendMsg {
	
    public String toJson() {
        return new Gson().toJson(new MsgWrapper(this));
    }

    protected Object msgData() {
        return this;
    }

    protected abstract String msgType();
    
}

class MsgWrapper {
    public final String msgType;
    public final Object data;
    
    MsgWrapper(final String msgType, final Object data) {
        this.msgType = msgType;
        this.data = data;
    }

    public MsgWrapper(final SendMsg sendMsg) {
        this(sendMsg.msgType(), sendMsg.msgData());
    }
}

class Join extends SendMsg {
    public final String name;
    public final String key;

    Join(final String name, final String key) {
        this.name = name;
        this.key = key;
    }

    @Override
    protected String msgType() {
        return "join";
    }
}

class Ping extends SendMsg {

	@Override
    protected String msgType() {
        return "ping";
    }
}

class YourCar {
	public String name;
	public String color;
}

class SwitchLane extends SendMsg {
	private String value;
	
	public SwitchLane(String value) {
		this.value = value;
	}
	
	@Override
    protected Object msgData() {
        return value;
    }

	@Override
	protected String msgType() {
		return "switchLane";
	}
}

class NewRace  {
	public BotID botId;
	public String trackName;
	public String password;
	public int carCount;
}

class BotID {
	public String name;
	public String key;
}

class CreateRace extends SendMsg {
	private NewRace value;
	
	public CreateRace(NewRace value) {
		this.value = value;
	}
	
	@Override
    protected Object msgData() {
        return value;
    }

	@Override
	protected String msgType() {
		return "createRace";
	}
}

class JoinRace extends SendMsg {
	private NewRace value;
	
	public JoinRace(NewRace value) {
		this.value = value;
	}
	
	@Override
    protected Object msgData() {
        return value;
    }

	@Override
	protected String msgType() {
		return "joinRace";
	}
}

class Turbo extends SendMsg {
	private String value;
	
	public Turbo(String value) {
		this.value = value;
	}
	

	@Override
    protected Object msgData() {
        return value;
    }

	@Override
	protected String msgType() {
		return "turbo";
	}
}

class Throttle extends SendMsg {
    private double value;

    public Throttle(double throttle) {
    	this.value = throttle;
	}

	@Override
    protected Object msgData() {
        return value;
    }

    @Override
    protected String msgType() {
        return "throttle";
    }
}


class MsgWrapperDeserializer implements JsonDeserializer<MsgWrapper> {
	
	Gson gson = new Gson();

	@Override
	public MsgWrapper deserialize(JsonElement arg0, Type arg1,
			JsonDeserializationContext arg2) throws JsonParseException {
		
		JsonObject json = arg0.getAsJsonObject();
		JsonElement type = json.get("msgType");
		
		if(arg1 == null)
			return gson.fromJson(arg0, arg1);
		
		String msgType = type.getAsString();
		Object data = null;
		
		switch(msgType) {
			case "yourCar":
				data = gson.fromJson(json.get("data"), YourCar.class);
				break;
			case "gameInit":
				data = gson.fromJson(json.get("data"), GameInit.class);
				break;
			case "gameEnd":
				data = gson.fromJson(json.get("data"), GameEnd.class);
				break;
			case "finish":
				data = gson.fromJson(json.get("data"), Car.class);
				break;
			case "crash":
				data = gson.fromJson(json.get("data"), Car.class);
				break;
			case "spawn":
				data = gson.fromJson(json.get("data"), Car.class);
				break;
			case "turboAvailable":
				data = gson.fromJson(json.get("data"), TurboInfo.class);
				break;
			case "lapFinished":
				data = gson.fromJson(json.get("data"), LapFinished.class);
				break;
			case "dnf":
				data = gson.fromJson(json.get("data"), DNF.class);
				break;
			case "carPositions":
				Type listType = new TypeToken<List<CarPosition>>() {}.getType();
				data = (gson.fromJson(json.get("data"),listType));
				break;
		}
		
		MsgWrapper message = new MsgWrapper(msgType, data);
		
		return message;
	}
}

class LapFinished {
	public Car car;
	public LapTime lapTime;
	public RaceTime raceTime;
	public Ranking ranking;
}

class LapTime {
	public int lap;
	public int ticks;
	public int millis;
}

class RaceTime {
	public int laps;
	public int ticks;
	public int millis;
}

class Ranking {
	public int overall;
	public int fastestLap;
}

class TurboInfo {
	public double turboDurationMilliseconds;
	public double turboDurationTicks;
	public double turboFactor;
}

class DNF {
	public Car car;
	public String reason;
}

class Car {
	public String name;
	public String color;
}

class GameEnd {
	public List<Results> results;
	public List<BestLap> bestLaps;
	
	class BestLap {
		public Car car;
		public Result result;
		
		class Result {
			public int lap;
			public int ticks;
			public int millis;
		}
	}
	
	class Results {
		public Car car;
		public Result result;
		
		class Result {
			public int laps;
			public int ticks;
			public int millis;
		}
	}
}

class CarPosition {
	public ID id;
	public double angle;
	public PiecePosition piecePosition;
}

class PiecePosition {
	public int pieceIndex;
	public double inPieceDistance;
	public Lane lane;
	public int lap;

	class Lane {
		public int startLaneIndex;
		public int endLaneIndex;
	}
}

class GameInit {
	public Race race;
	
	class Race {
		public Track track;
		public List<Car> cars;
		public RaceSession raceSession;
		
		class RaceSession {
			public int laps;
			public int maxLapTimeMs;
			public boolean quickrace;
		}
		
		class Car {
			public ID id;
			public Dimension dimensions;
			
			class ID {
				public String name;
				public String color;
			}
			
			class Dimension {
				public double length;
				public double width;
				public double guideFlagPosition;
			}
		}
		
		class Track {
			public String id;
			public String name;
			public List<Piece> pieces;
			public List<Lane> lanes;
			public StartingPoint startingPoint;
			
			class StartingPoint {
				public Position position;
				public double angle;
				
				class Position {
					public double x;
					public double y;
				}
			}
			
			class Piece {
				public double length;
				@SerializedName("switch")
				public boolean switchFlag;
				public double radius;
				public double angle;
			}
		}
		
		class Lane {
			public int distanceFromCenter;
			public int index;
		}
	}
	
}
